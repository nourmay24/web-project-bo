<?php
include_once '../../datasource.php';
include_once '../../models/category.php';
include_once '../../repositories/categoryRepository.php';
class CategoryService {
// required headers
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
 
// database connection will be here

// include database and object files

 
// instantiate database and product object
public $database; 
public $db; 
 
// initialize object
public $category; 
public $categoryRepository;

    // read products will be here
function read(){
    
   $this->database  = new Database();
   $this->db =  $this->database->getConnection();
   $this->category = new Category($this->db);
   $this->categoryRepository = new CategoryRepository($this->category);

   // query products
   $stmt =  $this->categoryRepository->read();
   $num = $stmt->rowCount();
 
   // check if more than 0 record found
   if($num>0){

       // products array
       $category_arr=array();
       $categories_arr["records"]=array();
 
       // retrieve our table contents
       // fetch() is faster than fetchAll()
       // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $category_item=array(
            "id" => $id,
            "name" => $name,
            "description" => html_entity_decode($description),
             "created" => $created,
             "modified" => $modified
            
        );
 
        array_push($categories_arr["records"], $category_item);
        }
 
    
        return $categories_arr;

      // set response code - 200 OK
      // http_response_code(200);
 
      // show products data in json format
      // echo json_encode($products_arr);
    }

}


function create($data){

   $this->database  = new Database();
   $this->db =  $this->database->getConnection();
   $this->category = new Category($this->db);
   $this->categoryRepository = new CategoryRepository($this->category);
 
    // create the product
   $stmt =  $this->categoryRepository->create($data);
 return $stmt;

}

}
 ?>