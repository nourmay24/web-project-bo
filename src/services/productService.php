<?php
include_once '../../datasource.php';
include_once '../../models/product.php';
include_once '../../repositories/productRepository.php';
class ProductService {
// required headers
//header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
 
// database connection will be here

// include database and object files

 
// instantiate database and product object
public $database; 
public $db; 
 
// initialize object
public $product; 
public $productRepository;

    // read products will be here
function read(){
    
   $this->database  = new Database();
   $this->db =  $this->database->getConnection();
   $this->product = new Product($this->db, null);
   $this->productRepository = new ProductRepository($this->product);

   // query products
   $stmt =  $this->productRepository->read();
   $num = $stmt->rowCount();
 
   // check if more than 0 record found
   if($num>0){

       // products array
       $products_arr=array();
       $products_arr["records"]=array();
 
       // retrieve our table contents
       // fetch() is faster than fetchAll()
       // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $product_item=array(
            "id" => $id,
            "name" => $name,
            "description" => html_entity_decode($description),
            "price" => $price,
            "category_id" => $category_id,
            "category_name" => $category_name,
             "created" => $created,
             "modified" => $modified
            
        );
 
        array_push($products_arr["records"], $product_item);
        }
 
    
        return $products_arr;

      // set response code - 200 OK
      // http_response_code(200);
 
      // show products data in json format
      // echo json_encode($products_arr);
    }

}


function create($data){

   $this->database  = new Database();
   $this->db =  $this->database->getConnection();
   $this->product = new Product($this->db, null);
   $this->productRepository = new ProductRepository($this->product);
 
   
    // create the product
   $stmt =  $this->productRepository->create($data);
   return $stmt;

}

}
 ?>
