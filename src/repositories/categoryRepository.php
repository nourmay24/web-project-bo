<?php

class CategoryRepository{

    protected $model;

    public function __construct(Category $model){
    
    $this->model = $model;
    
    }

// read products
public function read(){
 
    // select all query
          $query = "SELECT
                p.id, p.name, p.description, p.created, p.modified
            FROM
                " . $this->model->table_name . " p
               
            ORDER BY
                p.created DESC";
 
    // prepare query statement
    $stmt = $this->model->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
}
    
public function create ($data) {
    
        // query to insert record
    $query = "INSERT INTO
                " . $this->model->table_name . "
            SET
                name=:name, description=:description, created=:created";
 




    // prepare query
    $stmt = $this->model->conn->prepare($query);
 
    // sanitize
    $this->model->name=htmlspecialchars(strip_tags($data->name));
    $this->model->description=htmlspecialchars(strip_tags($data->description));
    $this->model->created=htmlspecialchars(strip_tags($data->created));
  //   $this->model->modified=htmlspecialchars(strip_tags($this->model->modified));

    // bind values
    $stmt->bindParam(":name", $this->model->name);
    $stmt->bindParam(":description", $this->model->description);
    $stmt->bindParam(":created", $this->model->created);
   //  $stmt->bindParam(":modified", $this->model->modified);

    // execute query
    $stmt->execute();
   
    
 
    return "ok";
    }
         

// used when filling up the update product form
function readOne(){
 
    // query to read single record
    $query = "SELECT
               p.id, p.name, p.description, p.created
            FROM
                " . $this->table_name . " p
        
            LIMIT
                0,1";
 
    // prepare query statement
    $stmt = $this->model->conn->prepare( $query );
 
    // bind id of product to be updated
    $stmt->bindParam(1, $this->id);
 
    // execute query
    $stmt->execute();
 
    // get retrieved row
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
    // set values to object properties
    $this->name = $row['name'];
    $this->description = $row['description'];
}


// update the product
function update(){
 
    // update query
    $query = "UPDATE
                " . $this->table_name . "
            SET
                name = :name,
                description = :description,
            WHERE
                id = :id";
 
    // prepare query statement
    $stmt = $this->model->conn->prepare($query);
 
    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->description=htmlspecialchars(strip_tags($this->description));
    $this->id=htmlspecialchars(strip_tags($this->id));
 
    // bind new values
    $stmt->bindParam(':name', $this->name);
    $stmt->bindParam(':description', $this->description);
    $stmt->bindParam(':id', $this->id);
 
    // execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}

// delete the product
function delete(){
 
    // delete query
    $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $this->id=htmlspecialchars(strip_tags($this->id));
 
    // bind id of record to delete
    $stmt->bindParam(1, $this->id);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
 
    return false;
     
}

// search products
function search($keywords){
 
    // select all query
    $query = "SELECT
                p.id, p.name, p.description, p.created
            FROM
                " . $this->table_name . " p
                
            WHERE
                p.name LIKE ? OR p.description LIKE ? OR c.name LIKE ?
            ORDER BY
                p.created DESC";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $keywords=htmlspecialchars(strip_tags($keywords));
    $keywords = "%{$keywords}%";
 
    // bind
    $stmt->bindParam(1, $keywords);
    $stmt->bindParam(2, $keywords);
    $stmt->bindParam(3, $keywords);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
}

// read products with pagination
public function readPaging($from_record_num, $records_per_page){
 
    // select query
    $query = "SELECT
               p.id, p.name, p.description,  p.created
            FROM
                " . $this->table_name . " p
            
            ORDER BY p.created DESC
            LIMIT ?, ?";
 
    // prepare query statement
    $stmt = $this->model->conn->prepare( $query );
 
    // bind variable values
    $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
    $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
 
    // execute query
    $stmt->execute();
 
    // return values from database
    return $stmt;
}

// used for paging products
public function count(){
    $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
 
    $stmt = $this->conn->prepare( $query );
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
    return $row['total_rows'];
}
}

?>