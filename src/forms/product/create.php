<?php
include_once '../../datasource.php';

    include_once '../../controllers/productController.php'; 
 include_once '../../models/product.php';

$productController = new ProductController();
 $database  = new Database();
   $db =  $database->getConnection();
   $data =  new \stdClass();

if (isset($_POST["name"]))
    $data->name= $_POST['name'];
if (isset($_POST["description"]))   
    $data->description = htmlentities(trim( $_POST['description']));
if (isset($_POST["price"]))
    $data->price= htmlentities(trim( $_POST['price']));
if (isset($_POST["category"]))
    $data->category_id= htmlentities(trim( $_POST['category']));

 $product = new Product($db, $data);   
    
$result = $productController->createCall($product);   

 return $result;  


?>