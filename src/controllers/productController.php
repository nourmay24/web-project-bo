<?php    
    include_once '../../services/productService.php';

class ProductController
{
    public $service;
    public  $result;
    public function __construct() // or any other method
    {
     $this->service= new productService();
    }
   

   function readCall(){
 
     $this->result =  $this->service->read();
    return  $this->result;

   }
   function createCall($data) {
       
          $data->created = date('Y-m-d H:i:s');
       $this->result =  $this->service->create($data);
       
    return  $this->result;

       
   }
}
?>