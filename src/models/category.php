<?php
class Category{
 
    // database connection and table name
    public $conn;
    public  $table_name = "categories";
 
    // object properties
    public $id;
    public $name;
    public $description;
    public $created;
    public $modified;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
}