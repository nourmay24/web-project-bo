<?php
class Product{
 
    // database connection and table name
    public $conn;
    public  $table_name = "products";
 
    // object properties
    public $id;
    public $name;
    public $description;
    public $price;
    public $category_id;
    public $category_name;
    public $created;
    public $modified;
 
    // constructor with $db as database connection
    /*public function __construct($db){
        $this->conn = $db;
    }
*/
       // constructor with $db as database connection and data
    public function __construct($db, $data){
        $this->conn = $db;
        if ($data !== NULL){
            if(isset($data->name))
        $this->name = $data->name;
           if(isset($data->description))
        $this->description = $data->description;
           if(isset($data->price))
        $this->price = $data->price;
           if(isset($data->category_id))
        $this->category_id = $data->category_id;
        }
      $this->created = date('Y-m-d H:i:s');

    }
}